import React from 'react';
import './style.css';

const Snack = (props) => {
  const styleAnimating = props.styleAnimating

  return (
    <span className="text-animation-container">
      <ul className={ `text-animation ${styleAnimating}` }>
        <li className="snack">snack</li>
        <li className="stack">stack</li>
      </ul>
    </span>
  )
}

Snack.defaultProps = {
  styleAnimating: ''
}

export default Snack;