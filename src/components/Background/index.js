import React, { Component } from 'react';
import './style.css';

export default class Background extends Component {
  state = {
    x: 0,
    y: 0
  }

  catchEvent = (e) => {
    let axisY = (window.innerHeight / 2 + 100) - e.screenY
    let axisX = (window.innerWidth / 2) - e.screenX

    this.setState({ x: axisX / 3, y: axisY / 4 })
  }

  render() {
    return (
      <div onMouseMove={ this.catchEvent } className="wrapper-background">
        <div style={{ width: 100, height: 100, background: '#1A1D24', transform: `translate(${this.state.x}px, ${this.state.y}px)`, transition: 'all 1500ms liniear' }}></div>
      </div>
    );
  }
}
