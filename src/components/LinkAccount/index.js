import React from 'react';
import './style.css';

const LinkAccount = (props) => {
  const styleAnimating = props.styleAnimating

  return (
    <div className={ `speech-bubble ${styleAnimating}` }>
      <ul className="links">
        <li className="link">
          <a href="http://gitlab.com/alisyahidin" rel="noopener noreferrer" target="_blank"><i className="fa fa-gitlab"></i></a>
        </li>
        <li className="link">
          <a href="http://github.com/alisyahidin" rel="noopener noreferrer" target="_blank"><i className="fa fa-github"></i></a>
        </li>
        <li className="link">
          <a href="https://www.linkedin.com/in/alisyahidin" rel="noopener noreferrer" target="_blank"><i className="fa fa-linkedin"></i></a>
        </li>
        <span className="divider">|</span>
        <li className="link">
          <a href="https://www.facebook.com/AliSyahidin19" rel="noopener noreferrer" target="_blank"><i className="fa fa-facebook"></i></a>
        </li>
        <li className="link">
          <a href="http://instagram.com/alisyahidin_" rel="noopener noreferrer" target="_blank"><i className="fa fa-instagram"></i></a>
        </li>
      </ul>
    </div>
  )
}

LinkAccount.defaultProps = {
  styleAnimating: ''
}

export default LinkAccount;