import React, { Component } from 'react';
import { store } from '../../store';
import './style.css';

export default class Loader extends Component {
  state = {
    animation: 'prepare'
  }

  setAnimation = (style = '') => {
    this.setState({ animation: style })
  }

  componentDidMount() {
    setTimeout(this.setAnimation('start'), 100)
  }

  render() {
    const { bgColor } = store.getState().background
    const { animation } = this.state
    const { stop } = this.props

    return (
      <div className={ `loader-wrapper ${stop?'hide':''}` }  style={{ background: bgColor }}>
        <div className={ `loader ${animation} ${stop?'stop':''}` } onMouseEnter={ this.animateAll } onMouseLeave={ this.animateAll }>
          <svg viewBox="0 0 129 129" width="25" height="25">
            <path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z" fill="#FFFFFF"/>
          </svg>
        </div>
      </div>
    )
  }
}