import React from 'react';
import { store } from '../../../store';
import './style.css';

const Menu = (props) => {
  const { open, toggleMenu } = props
  const { background } = store.getState()

  return (
    <div className="menu-wrapper">
      <div onClick={ toggleMenu } id="menu" className={ open ? 'open-menu' : 'close-menu' } style={{ background: background.menuColor }} />
      <ul className="menus">
        <li className={ `menu-item ${open ? 'item-show' : 'item-hide' }`}>
          <a href="#">
            Home
          </a>
        </li>
        <li className={ `menu-item ${open ? 'item-show' : 'item-hide' }`}>
          <a href="#">
            About
          </a>
        </li>
        <li className={ `menu-item ${open ? 'item-show' : 'item-hide' }`}>
          <a href="#">
            Contact
          </a>
        </li>
      </ul>
    </div>
  )
}

export default Menu;