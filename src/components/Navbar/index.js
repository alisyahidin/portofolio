import React, { Component } from 'react';
import Menu from './Menu';
import './style.css';

export default class Navbar extends Component {
  state = {
    openMenu: false
  }

  toggleMenu = () => {
    this.setState(prevState => ({ openMenu: !prevState.openMenu}))
  }

  render() {
    return (
      <div className="navbar-container">
        <Menu open={ this.state.openMenu } toggleMenu={ this.toggleMenu }/>
        <div className='navbar'>
          <div id="nav-icon-container">
            <div onClick={ this.toggleMenu } id="nav-icon" className={ this.state.openMenu ? 'open' : ''}>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}