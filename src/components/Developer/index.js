import React from 'react';
import Browser from './Browser';
import './style.css';

const Developer = (props) => {
  const developer = [
    'd', 'e', 'v', 'e', 'l', 'o', 'p', 'e', 'r'
  ]
  const { styleAnimating } = props

  return (
    <span className="developer">
      { developer.map((letter, index) => {
        return letter === 'o' ? (
          <span key={ index } className="flip-animate"><span className={ styleAnimating } data-hover={ letter }><Browser/></span></span>
        ) : ( <span key={ index } className="flip-animate"><span className={ styleAnimating } data-hover={ letter }>{ letter }</span></span> )
      }) }
      .
    </span>
  )
}

Developer.defaultProps = {
  styleAnimating: ''
}

export default Developer;