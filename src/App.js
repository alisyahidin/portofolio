import React, { Component } from 'react';
import { Home, NotFound } from './pages';
import { Router } from "@reach/router";

import { store } from './store';

class App extends Component {
  render() {
    const { background } = store.getState()

    return (
      <Router className="body" style={{ background: background.bgColor }}>
        <Home path="/" />
        <NotFound default />
      </Router>
    );
  }
}

export default App;