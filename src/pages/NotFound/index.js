import React, { Component } from 'react';
import './style.css';

export default class NotFound extends Component {
  render() {
    return (
      <div className="container flex">
        <h1>404</h1>
      </div>
    );
  }
}