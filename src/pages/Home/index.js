import React, { Component } from 'react';
import { Transition } from 'react-transition-group';
import {
  LinkAccount,
  Earth,
  Snack,
  Footer,
  Navbar,
  Developer,
  Loader } from '../../components';
import './style.css';

export default class Home extends Component {
  state = {
    linkAccount: false,
    earth: false,
    snack: false,
    developer: false,
    loading: true
  }

  animateLinkAccout = () => {
    this.setState(prevState => ({ linkAccount: !prevState.linkAccount }))
  }

  animateEarth = () => {
    this.setState(prevState => ({ earth: !prevState.earth }))
  }

  animateSnack = () => {
    this.setState(prevState => ({ snack: !prevState.snack }))
  }

  animateDeveloper = () => {
    this.setState(prevState => ({ developer: !prevState.developer }))
  }

  animateAll = () => {
    this.setState(prevState => ({
      linkAccount: !prevState.linkAccount,
      earth: !prevState.earth,
      snack: !prevState.snack,
      developer: !prevState.developer,
    }))
  }

  componentDidMount = () => {
    window.addEventListener('load', this.stopLoading)
  }

  stopLoading = () => {
    setTimeout(() => {
        this.setState(prevState => ({ loading: !prevState.loading}))
      }, 2500)
  }

  render() {
    const { loading } = this.state

    return (
      <div className="wrapper flex flex-column">
        { <Loader stop={ !loading }/> }
        <Navbar />
        <div className="text-container">
          <span className="text">Hello&nbsp;</span>
          <span onMouseEnter={ this.animateEarth } onMouseLeave={ this.animateEarth } className="world text">
            w
            <Transition
              in={ this.state.earth }
              timeout={ 550 }
            >
              {state => {
                switch (state) {
                  case 'entering':
                    return <Earth styleAnimating="earth-animating" />
                  case 'entered':
                    return <Earth styleAnimating="earth-animating" />
                  default:
                    return <Earth />
                }
              }}
            </Transition>
            rld,&nbsp;
          </span>
          <span className="text">i am&nbsp;</span>
          <span className="text-highlight" onMouseEnter={ this.animateLinkAccout } onMouseLeave={ this.animateLinkAccout }>
            <Transition
              in={ this.state.linkAccount }
              timeout={ 695 }
            >
              {state => {
                switch (state) {
                  case 'entering':
                    return <LinkAccount styleAnimating="showing"/>
                  case 'entered':
                    return <LinkAccount styleAnimating="show"/>
                  case 'exiting':
                    return <LinkAccount styleAnimating="hiding"/>
                  default:
                    return <LinkAccount/>
                }
              }}
            </Transition>
            <span className="text-highlight text">Ali Syahidin</span>
          </span>
          <br />
          <span className="text">
            <span className="fullstack-container" onMouseEnter={ this.animateSnack } onMouseLeave={ this.animateSnack }>
              a full
              <Transition
                in={ this.state.snack }
                timeout={ 600 }
              >
                {state => {
                  switch (state) {
                    case 'entering':
                      return <Snack styleAnimating="text-animating" />
                    case 'entered':
                      return <Snack styleAnimating="text-animating" />
                    default:
                      return <Snack/>
                  }
                }}
              </Transition>
            </span>
            &nbsp;web&nbsp;
            <span onMouseEnter={ this.animateDeveloper } onMouseLeave={ this.animateDeveloper }>
              <Transition
                in={ this.state.developer }
                timeout={ 400 }
              >
                {state => {
                  switch (state) {
                    case 'entering':
                      return <Developer styleAnimating="flip" />
                    case 'entered':
                      return <Developer styleAnimating="flip" />
                    default:
                      return <Developer />
                  }
                }}
              </Transition>
            </span>
          </span>
        </div>
        <div className="button" onMouseEnter={ this.animateAll } onMouseLeave={ this.animateAll }>
          <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 129 129" enableBackground="new 0 0 129 129" width="25" height="25">
            <path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z" fill="#FFFFFF"/>
          </svg>
        </div>

        <Footer />
      </div>
    );
  }
}
