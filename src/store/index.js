import { createStore, combineReducers } from 'redux';
import backgroundReducer from '../reducers/backgroundReducer';

export const store = createStore(
  combineReducers({
    background: backgroundReducer
  })
);