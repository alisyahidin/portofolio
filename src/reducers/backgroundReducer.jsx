const colors = [
  {
    bgColor: '#3F729B',
    menuColor: '#1C2331'
  },
  {
    bgColor: '#303030',
    menuColor: '#212121'
  },
  {
    bgColor: '#e53935',
    menuColor: '#A02222'
  },
]

const index = Math.floor(Math.random() * Math.floor(colors.length));

let defaultState = {
  bgColor: colors[index].bgColor,
  menuColor: colors[index].menuColor
}

const backgroundReducer = ( state = defaultState, action ) => {
  switch (action.type) {
    case 'CHANGE-BG':
      return {
        ...state
      }
    default: return state;
  }
}

export default backgroundReducer;